<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
    	'customer_id',
	    'amount',
	    'payment_date',
    ];

    public function customer(){
    	return $this->belongsTo('App\Customer');
    }
}
