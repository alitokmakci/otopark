<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
    	'name',
	    'plate',
	    'tel',
	    'model',
	    'price',
    ];

    public function payments(){
    	return $this->hasMany('App\Payment');
    }

    public function checkIn(){
    	return $this->hasOne('App\CheckIn');
    }

    public function sms(){
    	return $this->hasMany('App\SmsStatus');
    }
}
