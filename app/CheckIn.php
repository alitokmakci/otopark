<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckIn extends Model
{
    protected $fillable = [
    	'check_in',
	    'check_out',
	    'customer_id'
    ];

    public function customer(){
    	return $this->belongsTo('App\Customer');
    }
}
