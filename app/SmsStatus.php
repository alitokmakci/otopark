<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsStatus extends Model
{
    protected $fillable = [
    	'customer_id',
	    'sid',
	    'status',
	    'content'
    ];
}
