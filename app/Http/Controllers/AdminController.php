<?php

namespace App\Http\Controllers;

use App\CheckIn;
use App\Customer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    public function welcome(){
        return view('backend.welcome');
    }

    public function allTables()
    {
        return view('backend.tables');
    }


    public function login()
    {
    	if(session()->has('user')){
    		return redirect('/yonetim');
	    }
    	return view('backend.login');
    }

    public function tryLogin(Request $request)
    {
    	if(User::where('name', $request->username)->where('password', $request->password)->exists()){
    		session()->put('user', User::where('name', $request->username)->where('password', $request->password)->first()->id);
    		return view('backend.welcome');
    	} else {
    		return redirect('/login');
    	}
    }

    public function siteSettings(){
    	return view('backend.siteSettings');
    }

	public function gallerySettings(){
		return view('backend.gallerySettings');
	}

	public function tomorrow(){
    	$checks = CheckIn::whereDay('check_in', Carbon::tomorrow()->day)->where('check_out', null)->distinct('customer_id')->get();
    	return view('backend.tomorrow')->with('checks', $checks);
	}

	public function customer($id){
    	return view('backend.customer')->with('customer', Customer::find($id));
	}

	public function otherSettings(){
    	return view('backend.otherSettings');
	}

	public function smsTracking(){
    	return view('backend.smsTrack');
	}
}
