<?php

	namespace App\Http\Controllers;

	use App\CheckIn;
	use App\Customer;
	use App\Gallery;
	use App\Payment;
	use App\Service;
	use App\SiteSettings;
	use App\SmsStatus;
	use App\Twilio;
	use Carbon\Carbon;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\App;
	use Illuminate\Support\Facades\Storage;
	use Illuminate\Support\Str;
	use Twilio\Rest\Client;

	class ApiController extends Controller
	{
		public function getData(Request $request)
		{
			$checks = CheckIn::whereYear('check_in', '<=', $request->year)
				->where(function ($q) use ($request) {
					$q
						->whereYear('check_out', '>=', $request->year)
						->orWhere('check_out', null);
				})
				->orderBy('customer_id')
				->orderBy('check_in', 'ASC')
				->distinct('customer_id')
				->get();
			$customers = array();
			foreach ($checks as $check) {
				setlocale(LC_TIME, 'tr_TR');
				$date = Carbon::parse($check->check_in);
				$check->customer->check_in = $date->formatLocalized('%d %B %Y');
				if ($check->check_out != null) {
					$check->customer->check_out = Carbon::parse($check->check_out)->formatLocalized('%d %B %Y');
				} else {
					$check->customer->check_out = null;
				}
				array_push($customers, $check->customer);
				$i = 1;
				$customerPayments = array();
				while ($i <= 12) {
					$payments = Payment::where('customer_id', $check->customer->id)->whereYear('payment_date', $request->year)->whereMonth('payment_date', $i)->get();
					$total = 0;

					foreach ($payments as $payment) {
						$total += $payment->amount;
					}
					if (($i < Carbon::parse($check->check_in)->month AND $request->year == Carbon::parse($check->check_in)->year) OR ((Carbon::parse($check->check_out)->month < $i AND $request->year == Carbon::parse($check->check_out)->year) AND $check->check_out != null)) {
						$total = 'X';
					}
					if ($check->check_out != null AND ($request->year == Carbon::parse($check->check_out)->year AND Carbon::parse($check->check_in)->day >= Carbon::parse($check->check_out)->day AND $i == Carbon::parse($check->check_out)->month)) {
						$total = 'X';
					}
					array_push($customerPayments, $total);
					$i++;
				}

				$check->customer->yearpayments = $customerPayments;
			}
			return response()->json(['customers' => $customers]);
		}

		public function getYears(Request $request)
		{
			$years = array();
			$first_year_check = CheckIn::orderBy('check_in', 'ASC')->get()->first();
			$first_year = Carbon::parse($first_year_check->check_in)->year;
			while ($first_year <= Carbon::today()->year) {
				array_push($years, $first_year);
				$first_year++;
			}
			rsort($years);
			return response()->json(['years' => $years]);
		}

		public function addPayment(Request $request)
		{
			$today = Carbon::today();
			$date = Carbon::parse($today->day . '-' . $request->month . '-' . $request->year);
			if(Payment::where('customer_id', $request->customer_id)->whereYear('payment_date', $request->year)->whereMonth('payment_date', $request->month)->exists()){
				Payment::where('customer_id', $request->customer_id)->whereYear('payment_date', $request->year)->whereMonth('payment_date', $request->month)->update(array(
					'amount' => $request->amount
				));
			} else {
				Payment::create(array(
					'customer_id' => $request->customer_id,
					'amount' => $request->amount,
					'payment_date' => $date
				));
			}

			return response()->json(['result' => 'success']);
		}

		public function deleteCustomer(Request $request)
		{
			Customer::where('id', $request->id)->delete();
			return response()->json(['result' => 'success']);
		}

		public function checkOut(Request $request)
		{
			CheckIn::where('customer_id', $request->id)->update([
				'check_out' => Carbon::parse($request->date),
			]);

			return response()->json(['result', 'success']);
		}

		public function makeDate(Request $request)
		{
			setlocale(LC_TIME, 'tr_TR');
			return response()->json(['date' => Carbon::parse($request->date)->formatLocalized('%d %B %Y'), 'month' => Carbon::parse($request->date)->month, 'year' => Carbon::parse($request->date)->year]);
		}

		public function addNewCustomer(Request $request)
		{
			$customer = Customer::create(array(
				'name' => $request->customer['name'],
				'plate' => $request->customer['plate'],
				'model' => $request->customer['model'],
				'tel' => $request->customer['tel'],
				'price' => $request->customer['price'],
			));
			if (isset($request->customer['check_out'])) {
				CheckIn::create(array(
					'check_in' => Carbon::parse($request->customer['check_in']),
					'check_out' => Carbon::parse($request->customer['check_out']),
					'customer_id' => $customer->id
				));
			} else {
				CheckIn::create(array(
					'check_in' => Carbon::parse($request->customer['check_in']),
					'customer_id' => $customer->id
				));
			}
			$i = 0;
			$day = Carbon::parse($request->customer['check_in'])->day;
			while ($i < 12) {
				$k = $i + 1;
				$date = Carbon::parse($day . '-' . $k . '-' . $request->year);
				if ($request->customer['yearpayments'][$i] != null) {

					Payment::create(array(
						'customer_id' => $customer->id,
						'amount' => $request->customer['yearpayments'][$i],
						'payment_date' => $date,
					));
				}
				$i++;
			}

			return response()->json([
				'result' => 'success',
			]);
		}

		public function smsStatus(Request $request)
		{
			SmsStatus::where('sid', $request->SmsSid)->update([
				'status' => $request->SmsStatus
			]);
			return response();
		}

		public function sendSMS(Request $request)
		{
			$sid = Twilio::find(1)->sid;
			$token = Twilio::find(1)->token;
			$from = Twilio::find(1)->number;
			$twilio = new Client($sid, $token);

			$message = $twilio->messages->create('+9' . $request->tel, // to
				array(
					"body" => $request->text,
					"from" => $from,
					"statusCallback" => App::make('url')->to('/api/sms_status')
				)
			);

			SmsStatus::create(array(
				'customer_id' => $request->id,
				'sid' => $message->sid,
				'status' => 'waiting',
				'content' => $request->text
			));

			return response()->json(['result' => 'sended']);
		}

		public function daily_car_rate()
		{
			$i = 0;
			setlocale(LC_TIME, 'tr_TR');
			$data = array();
			$days = array();
			while ($i < 11) {
				$total = 0;
				foreach (Payment::whereDate('payment_date', Carbon::today()->subDays($i))->get() as $payment) {
					$total += $payment->amount;
				}
				$data[$i] = $total;
				$days[$i] = Carbon::today()->subDays($i)->formatLocalized('%d %B %Y');
				$i++;
			}
			return response()->json(['data' => $data, 'days' => $days]);
		}

		public function welcome_chart(Request $request)
		{
			$customer_counter_array = array();
			$payment_counter_array = array();
			$date_array = array();
			$i = 11;
			while ($i >= 0) {
				$customer_counter = 0;
				$payment_counter = 0;
				foreach (CheckIn::whereMonth('check_in', '=', Carbon::today()->subMonths($i))->whereYear('check_in', Carbon::today()->subMonths($i))->get() as $check) {
					$customer_counter += 1;
				}
				foreach (Payment::whereMonth('payment_date', Carbon::today()->subMonths($i))->whereYear('payment_date', Carbon::today()->subMonths($i))->get() as $payment) {
					if ($payment->amount > 0) {
						$payment_counter += 1;
					}
				}
				array_push($date_array, Functions::date_formatter_for_chart(Carbon::today()->subMonths($i)));
				array_push($payment_counter_array, $payment_counter);
				array_push($customer_counter_array, $customer_counter);
				$i--;
			}
			return response()->json(['customer_counter' => $customer_counter_array, 'payment_counter' => $payment_counter_array, 'date' => $date_array]);
		}

		public function update_tel(Request $request)
		{
			Customer::where('id', $request->id)->update([
				'tel' => $request->tel
			]);

			return response()->json(['result' => 'success']);
		}

		public function upload(Request $request)
		{
			// Creating a new time instance, we'll use it to name our file and declare the path
			$time = Carbon::now();
			// Requesting the file from the form
			$image = $request->file('file');
			// Getting the extension of the file
			$extension = $image->getClientOriginalExtension();
			// Creating the directory, for example, if the date = 18/10/2017, the directory will be 2017/10/
			$directory = '';
			// Creating the file name: random string followed by the day, random number and the hour
			$filename = Str::random(5) . date_format($time, 'd') . rand(1, 9) . date_format($time, 'h') . "." . $extension;
			// This is our upload main function, storing the image in the storage that named 'public'
			$upload_success = $image->storeAs($directory, $filename, 'public');
			// If the upload is successful, return the name of directory/filename of the upload.
			if ($upload_success) {
				if (Gallery::get()->count() > 0) {
					$last_img = Gallery::orderBy('img_order', 'DESC')->get()->first();
					$order = $last_img->img_order + 1;
				} else {
					$order = 1;
				}
				$src = $upload_success;
				$new_img = Gallery::create(array(
					'src' => $src,
					'img_order' => $order,
				));
				return response()->json(['src' => $upload_success, 'order' => $order, 'id' => $new_img->id]);
			} // Else, return error 400
			else {
				return response()->json('error', 400);
			}
		}

		public function img_order_save(Request $request)
		{
			Gallery::where('id', $request->id)->update([
				'img_order' => $request->order,
			]);

			return response()->json(['result' => 'success']);
		}

		public function change_img_name(Request $request)
		{
			Gallery::where('id', $request->img_id)->update([
				'name' => $request->name,
			]);
			return response()->json(['data' => 'success']);
		}

		public function delete_img(Request $request)
		{
			Gallery::where('id', $request->img_id)->delete();
			$i = 1;
			foreach (Gallery::orderBy('img_order')->get() as $img) {
				Gallery::where('id', $img->id)->update([
					'img_order' => $i
				]);
				$i++;
			}
			return response()->json(['result' => 'success']);
		}

		public function saveSettings(Request $request){
			SiteSettings::where('id', 1)->update(array(
				'name' => $request->name,
				'keywords' => $request->keywords,
				'description' => $request->description,
				'slogan' => $request->slogan,
				'about_us_1' => $request->about_us_1,
				'about_us_title_1' => $request->about_us_title_1,
				'footer_text' => $request->footer_text,
				'facebook' => $request->facebook,
				'instagram' => $request->instagram,
				'whyus_1' => $request->whyus_1,
				'whyus_2' => $request->whyus_2,
				'whyus_3' => $request->whyus_3,
				'whyus_content_1' => $request->whyus_content_1,
				'whyus_content_2' => $request->whyus_content_2,
				'whyus_content_3' => $request->whyus_content_3
			));

			return response()->json([], 200);
		}

		public function getGallery(Request $request){
			return response()->json(['images' => Gallery::orderBy('img_order')->get()]);
		}

		public function uploadAbout(Request $request){
			// Creating a new time instance, we'll use it to name our file and declare the path
			$time = Carbon::now();
			// Requesting the file from the form
			$image = $request->file('file');
			// Getting the extension of the file
			$extension = $image->getClientOriginalExtension();
			// Creating the directory, for example, if the date = 18/10/2017, the directory will be 2017/10/
			$directory = '';
			// Creating the file name: random string followed by the day, random number and the hour
			$filename = Str::random(5) . date_format($time, 'd') . rand(1, 9) . date_format($time, 'h') . "." . $extension;
			// This is our upload main function, storing the image in the storage that named 'public'
			$upload_success = $image->storeAs($directory, $filename, 'public');
			// If the upload is successful, return the name of directory/filename of the upload.
			if ($upload_success) {
				$new_img = SiteSettings::where('id', 1)->update(array(
					'about_us_img_1' => $upload_success,
				));
				return response()->json(['src' => $upload_success]);
			} // Else, return error 400
			else {
				return response()->json('error', 400);
			}
		}

		public function getServices(){
			return response()->json(Service::orderBy('id')->get());
		}

		public function addService(Request $request){
			Service::create(array(
				'title' => $request->service_title,
				'content' => $request->service_content,
			));
			return response()->json([],200);
		}

		public function editService(Request $request){
			Service::where('id', $request->id)->update(array(
				'title' => $request->service_title,
				'content' => $request->service_content,
			));
			return response()->json([],200);
		}

		public function deleteService(Request $request){
			Service::where('id', $request->id)->delete();
			return response()->json([],200);
		}

		public function saveOtherSettings(Request $request){
			Twilio::where('id', 1)->update(array(
				'sid' => $request->sid,
				'token' => $request->token,
				'number' => $request->number,
			));
			return response()->json([],200);
		}
	}
