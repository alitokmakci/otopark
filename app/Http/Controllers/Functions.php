<?php

	namespace App\Http\Controllers;

	use App\Customer;
	use Carbon\Carbon;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\DB;

	class Functions extends Controller
	{
		public static function date_formatter_for_chart($date)
		{
			setlocale(LC_TIME, 'tr_TR');
			$date = Carbon::parse($date);
			return $date->formatLocalized('%B %Y');
		}

		public static function date_formatter($date)
		{
			setlocale(LC_TIME, 'tr_TR');
			$date = Carbon::parse($date);
			return $date->formatLocalized('%d %B %Y');
		}

		public static function date_formatter_with_hour($date)
		{
			setlocale(LC_TIME, 'tr_TR');
			$date = Carbon::parse($date);
			return $date->formatLocalized('%d %B %Y - %H:%M');
		}

		public static function total_dept($id)
		{
			$customer = Customer::find($id);
			$check_in = Carbon::parse($customer->checkIn->check_in);
			$check_out = Carbon::parse($customer->checkIn->check_out);
			if ($check_out == null) {
				$i = 0;
				$months = 0;
				while (Carbon::parse($customer->checkIn->check_in)->addMonths($i) < Carbon::today()) {
					$months += 1;
					$i++;
				}
			} else {
				$i = 0;
				$months = 0;
				while (Carbon::parse($customer->checkIn->check_in)->addMonths($i) < Carbon::parse($customer->checkIn->check_out)) {
					$months += 1;
					$i++;
				}
			}
			return $months * $customer->price;
		}

		public static function total_payments($id)
		{
			$customer = Customer::find($id);
			$total = 0;
			foreach ($customer->payments as $payment) {
				$total += $payment->amount;
			}
			return $total;
		}

		public static function all_process($id)
		{
			$customer = Customer::find($id);
			$check_in = Carbon::parse($customer->checkIn->check_in);
			$check_out = Carbon::parse($customer->checkIn->check_out);
			DB::insert(DB::raw('DROP TABLE IF EXISTS customer_process'));
			DB::insert(DB::raw('CREATE TEMPORARY TABLE customer_process(
    			process VARCHAR(255),
    			content VARCHAR(255),
    			amount INT,
    			date DATE
			)'));
			if ($check_out == null) {
				$i = 0;
				while (Carbon::parse($customer->checkIn->check_in)->addMonths($i) < Carbon::today()) {
					$date = Carbon::parse($customer->checkIn->check_in)->addMonths($i);
					$content = Functions::date_formatter(Carbon::parse($customer->checkIn->check_in)->addMonths($i)).' - '.Functions::date_formatter(Carbon::parse($customer->checkIn->check_in)->addMonths($i+1)).' tarihleri arasında otopark kullanımı.';
					DB::insert(DB::raw("INSERT INTO customer_process
    				(process, content, amount, date) VALUES
    				('Aylık Abonelik', '$content', '$customer->price', '$date')"));
					$i++;
				}
			} else {
				$content = Functions::date_formatter(Carbon::parse($customer->checkIn->check_out)).' tarihinde otoparktan çıkış.';
				$check_out_date = Carbon::parse($customer->checkIn->check_out);
				DB::insert(DB::raw("INSERT INTO customer_process
    				(process, content, amount, date) VALUES
    				('Araç Çıkış', '$content', '0', '$check_out_date')"));

				$i = 0;
				$months = 0;
				while (Carbon::parse($customer->checkIn->check_in)->addMonths($i) < Carbon::parse($customer->checkIn->check_out)) {
					$date = Carbon::parse($customer->checkIn->check_in)->addMonths($i);
					$content = Functions::date_formatter(Carbon::parse($customer->checkIn->check_in)->addMonths($i)).' - '.Functions::date_formatter(Carbon::parse($customer->checkIn->check_in)->addMonths($i+1)).' tarihleri arasında otopark kullanımı.';
					DB::insert(DB::raw("INSERT INTO customer_process
    				(process, content, amount, date) VALUES
    				('Aylık Abonelik', '$content', '$customer->price', '$date')"));
					$i++;
				}
			}

			foreach ($customer->payments as $payment) {
				$content = Functions::date_formatter_for_chart($payment->payment_date).' tarihinde alınan ödeme.';
				if($payment->amount > 0){
					DB::insert(DB::raw("INSERT INTO customer_process
    				(process, content, amount, date) VALUES ('Ödeme', '$content', '$payment->amount', '$payment->payment_date')"));
				}
			}

			return DB::table('customer_process')->orderBy('date')->get();
		}

		public static function remainingDept($id){
			return Functions::total_dept($id) - Functions::total_payments($id);
		}
	}
