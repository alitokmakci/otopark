<h1 style="color: #8B1535">Otopark İçin Hazırlanan Laravel Projesi</h1>     
<h3>Aşağıdaki adımlar uygulanacak:</h3>
<ul>
    <li>Twilio account hazırla</li>
    <li>git pull origin master</li>
    <li>.env dosyası oluşturulup düzenlenecek</li>
    <li>composer install</li>
    <li>npm install</li>
    <li>npm run dev</li>
    <li>php artisan key:generate</li>
    <li>php artisan migrate</li>
    <li>php artisan storage:link</li>
</ul>