@extends('backend.app')

@section('content')
    <div class="container">
        <div class="row">
            @if(\App\SiteSettings::get()->count() == 0)
                <div class="col-12">
                    <div class="alert alert-danger" role="alert">
                        Lütfen Site Ayarlarını Kontrol Edin!
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                <div class="card bg-info text-white">
                    <div class="card-body">
                        <div class="dashboard-info row">
                            <div class="info-text col-md-12">
                                <h5 class="card-title">Tekrar Hoşgeldin {{\App\User::find(session('user'))->name}}!</h5>
                                <ul>
                                    <li><b>Yarın Ödeme Yapacaklar Sayfasından</b> yarın ödeme yapması beklenen müşterileri görebilirsiniz.</li>
                                    <li>Sistem <b>otomatik olarak her gün</b> araçların kontrolünü yapar ve abonelik yenilemelerini gerçekleştirir.
                                        Bu yüzden <b>çıkış yapan araçları mutlaka sisteme bildiriniz.</b></li>
                                    <li>Yarın ödeme yapması gereken müşterilere otomatik olarak sms göndermek için tablodan <b>hızlı sms</b> butonuna tıklamanız yeterlidir.</li>
                                    <li>Site Ayarları boş olduğunda web sitesi bozuk bir şekilde görünür mutlaka ayarları tamamen doldurunuz!</li>
                                    <li>Ayar sayfalarından tüm ayarları düzenleyebilirsiniz.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Müşteri Sayısı (Yıllık Grafik)</h5>
                        <div id="apex1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/backend/assets/plugins/chartjs/chart.min.js"></script>
    <script src="/backend/assets/plugins/apexcharts/dist/apexcharts.min.js"></script>

    <script>
        $(document).ready(function () {
            $.ajax({
                url: '/api/daily_car_rate',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    '_token': $('meta[name="_token"]').attr('content'),
                },
                success: function (e) {
                    var ctx = document.getElementById('visitorsChart');
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: e.days,
                            datasets: [{

                                label: 'Alınan Ödeme',
                                data: e.data,
                                backgroundColor: '#5780F7'
                            }]
                        },
                        options: {
                            responsive: true,
                            legend: {
                                display: 0 // place legend on the right side of chart
                            },
                            scales: {
                                yAxes: [{
                                    display: 0,
                                    stacked: true,
                                    ticks: {
                                        display: 0
                                    },
                                    gridLines: {
                                        color: "rgba(255,255,255,0)"
                                    }
                                }],
                                xAxes: [{
                                    display: 0,
                                    stacked: true,
                                    ticks: {
                                        display: 0
                                    },
                                    gridLines: {
                                        color: "rgba(255,255,255,0)"
                                    }
                                }]
                            }
                        }
                    });
                }
            })
        });
    </script>

    <script>
        $(document).ready(function () {
            $.ajax({
                url: '/api/welcome_chart',
                dataType: 'JSON',
                type: 'POST',
                data: {
                    '_token': $('meta[name="_token"]').attr('content'),
                    },
                success: function(e){
                    var options = {
                        chart: {
                            height: 350,
                            type: 'bar',
                            toolbar: {
                                show: false
                            },
                            foreColor: '#747985'
                        },
                        plotOptions: {
                            bar: {
                                horizontal: false,
                                columnWidth: '55%',
                                endingShape: 'rounded'
                            },
                        },

                        colors:['#5780F7', '#FFCD36', '#06BA54'],

                        dataLabels: {
                            enabled: false
                        },
                        stroke: {
                            show: true,
                            width: 2,
                            colors: ['transparent']
                        },
                        series: [{
                            name: 'Giriş Yapan Müşteri Sayısı',
                            data: e.customer_counter
                        }, {
                            name: 'Alınan Ödeme Sayısı',
                            data: e.payment_counter
                        }],
                        xaxis: {
                            categories: e.date,
                        },
                        fill: {
                            opacity: 1

                        },
                        tooltip: {
                            y: {
                                formatter: function (val) {
                                    return  val
                                }
                            }
                        },
                        grid: {
                            borderColor: '#747985'
                        }
                    }
                    var chart = new ApexCharts(
                        document.querySelector("#apex1"),
                        options
                    );

                    chart.render();
                }
            });
        });
    </script>
@endsection
