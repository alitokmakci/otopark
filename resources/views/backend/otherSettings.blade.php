@extends('backend.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">SMS Ayarları:</h5>
                <form id="settings-form">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Twilio SID:</label>
                                <input value="{{\App\Twilio::find(1)->sid}}" type="text" id="sid" name="sid" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="token">Twilio TOKEN:</label>
                                <input id="token" value="{{\App\Twilio::find(1)->token}}" type="text" name="token" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="token">Twilio Telefon:</label>
                                <input id="token" value="{{\App\Twilio::find(1)->number}}" type="text" name="number" class="form-control">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <button id="submit-btn" form="settings-form" type="button" class="btn btn-success p-3">Ayarları Kaydet
            </button>
        </div>
    </div>
@endsection

@section('js')
    <script>
        document.getElementById('submit-btn').addEventListener('click', function () {
            axios.post('/api/save-other-settings', {
                sid: document.getElementById('sid').value,
                token: document.getElementById('token').value,
            }).then(function (response) {
                swal.fire({
                    title: 'İşlem Başarılı!',
                    text: 'Ayarlar Başarıyla Kaydedildi!',
                    confirmButtonText: 'Tamam',
                    icon: 'success',
                });
            });
        });
    </script>
@endsection
