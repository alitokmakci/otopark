@extends('backend.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h3 class="">Müşteri Adı: <span class="ml-3">{{$customer->name}}</span> <span
                                    class="float-right text-info">Ödeme Beklenen Miktar: {{\App\Http\Controllers\Functions::total_dept($customer->id) - \App\Http\Controllers\Functions::total_payments($customer->id)}} ₺</span>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-2">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">İletişim Bilgileri:</h5>
                        <ul class="list-unstyled profile-about-list">
                            <li><i class="material-icons">local_phone</i><span
                                        id="customer_tel">{{$customer->tel}}</span></li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Kayıtlı Araçlar:</h5>
                        <div class="story-list">
                            <div class="story">
                                <div class="story-info">
                                    <strong class="">{{$customer->plate}}</strong>
                                </div>
                            </div>
                        </div>
                        @if($customer->checkIn->check_out == null)
                            <a onclick="checkOut({{$customer->id}})" href="#"
                               class="btn btn-block m-t-sm btn-primary">Çıkışını Yap</a>
                        @endif
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">İşlemler:</h5>
                        <ul class="list-unstyled profile-about-list">
                            <li>
                                <button onclick="sendSMS()" class="btn btn-block btn-primary m-t-lg"
                                        id="send_custom_sms">SMS Gönder
                                </button>
                                <button onclick="editCustomer()" class="btn btn-block btn-warning m-t-lg"
                                        id="customer_info_edit">Bilgilerini
                                    Düzenle
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-10">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">İşlemlerin ve Finansal Bilgilerin Dökümü:</h5>
                        <div class="row">
                            <div class="col-lg">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <div class="table-responsive m-t-xxl">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">İşlem</th>
                                                            <th scope="col">Plaka</th>
                                                            <th scope="col">İşlem Tarihi</th>
                                                            <th scope="col">İşlem Açıklama</th>
                                                            <th scope="col" class="text-right">Ücret</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach(\App\Http\Controllers\Functions::all_process($customer->id) as $item)
                                                            <tr>
                                                                <td>{{$item->process}}</td>
                                                                <td>{{$customer->plate}}</td>
                                                                <td>{{\App\Http\Controllers\Functions::date_formatter($item->date)}}</td>
                                                                <td>{{$item->content}}</td>
                                                                <td class="@if($item->process == 'Ödeme') text-success @elseif($item->process == 'Araç Çıkış') text-dark @else text-danger @endif text-right">
                                                                    <strong>{{$item->amount}} ₺</strong></td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="offset-md-8 col-md-4 m-t-xxl">
                                                <div class="invoice-info">
                                                    <p>Toplam Borç: <span>{{\App\Http\Controllers\Functions::total_dept($customer->id)}} ₺</span>
                                                    </p>
                                                    <p class="text-success">Toplam Alınan Ödeme: <span>{{\App\Http\Controllers\Functions::total_payments($customer->id)}} ₺</span>
                                                    </p>
                                                    <p class="bold text-danger">
                                                        Kalan Borç <span>{{\App\Http\Controllers\Functions::total_dept($customer->id) - \App\Http\Controllers\Functions::total_payments($customer->id)}} ₺</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function checkOut(id) {
            swal.fire({
                title: 'Dikkat!',
                text: '{{$customer->name}} adlı müşterinin aracının çıktığını onaylıyor musunuz?',
                input: 'text',
                inputPlaceholder: 'Tarihi GG-AA-YYYY şeklinde giriniz.',
                confirmButtonText: 'Onayla',
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'İptal'
            }).then((response) => {
                if (response.value) {
                    axios.post('/api/check_out', {
                        id: id,
                        date: response.value,
                    }).then((response) => {
                        swal.fire({
                            title: 'İşlem Başarılı',
                            text: '{{$customer->name}} adlı müşterinin aracı sistem çıkış yaptı!',
                            confirmButtonText: 'Tamam',
                            icon: 'success',
                        });
                    });
                }
            });
        }

        function editCustomer() {
            swal.fire({
                title: 'Telefon Güncelleme!',
                text: 'Müşterinin Telefonunu Girin',
                input: 'text',
                inputValue: '{{ $customer->tel }}',
                confirmButtonText: 'Onayla',
                showCancelButton: true,
                cancelButtonText: 'İptal'
            }).then((response) => {
                if (response.value) {
                    if (response.value !== '{{ $customer->tel }}') {
                        let new_tel = response.value;
                        axios.post('/api/update_tel', {
                            id: {{$customer->id}},
                            tel: new_tel
                        }).then((response) => {
                            document.getElementById('customer_tel').innerText = new_tel;
                            swal.fire({
                                title: 'İşlem Başarılı',
                                text: '{{$customer->name}} adlı müşterinin telefonu güncellendi!',
                                confirmButtonText: 'Tamam',
                                icon: 'success',
                            });
                        });
                    }
                }
            });
        }

        function sendSMS() {
            swal.fire({
                title: 'Göndermek İstediğniz Mesajı Giriniz.',
                input: 'textarea',
                inputValue: 'Sayın {{ $customer->name }}, ',
                confirmButtonText: 'Gönder',
                showCancelButton: true,
                cancelButtonText: 'İptal',
            }).then((res) => {
                if (res.value) {
                    axios.post('/api/send_sms', {
                        tel: '{{$customer->tel}}',
                        id: {{ $customer->id }},
                        text: res.value,
                    }).then((response) => {
                        swal.fire({
                            title: 'İşlem Başarılı',
                            text: '{{ $customer->name }} adlı müşteriye mesaj gönderildi!',
                            confirmButtonText: 'Tamam',
                            icon: 'success',
                        });
                    });
                }
            });

        }
    </script>
@endsection

@section('css')
    <meta name="_all_dept" content="">
@endsection
