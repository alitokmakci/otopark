@extends('backend.app')

@section('content')
    <div class="container-fluid">
        <table class="mt-5 table table-striped table-bordered text-center">
            <thead class="thead-dark">
            <tr>
                <th>Müşteri Adı</th>
                <th>Telefon Numarası</th>
                <th>Sms İçeriği</th>
                <th>Sms Durumu</th>
                <th>Gönderilme Tarihi</th>
            </tr>
            </thead>
            <tbody>
            @foreach(\App\SmsStatus::all() as $sms)
                <tr>
                    <td>{{\App\Customer::find($sms->customer_id)->name}}</td>
                    <td>{{\App\Customer::find($sms->customer_id)->tel}}</td>
                    <td>{{$sms->content}}</td>
                    <td>
                        @if($sms->status == 'Queued')
                            <span class="btn btn-info">Gönderilmeyi Bekliyor</span>
                        @elseif($sms->status == 'Sent')
                            <span class="btn btn-success">Gönderildi</span>
                        @elseif($sms->status == 'Delivered')
                            <span class="btn btn-success">İletildi</span>
                        @elseif($sms->status == 'Undelivered')
                            <span class="btn btn-warning">İletilmesi Bekleniyor</span>
                        @elseif($sms->status == 'Failed')
                            <span class="btn btn-danger">Gönderilemedi</span>
                        @else
                            <span class="btn btn-secondary">Hazırlanıyor</span>
                        @endif
                    </td>
                    <td>{{\App\Http\Controllers\Functions::date_formatter_with_hour($sms->created_at)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
