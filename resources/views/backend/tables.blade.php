@extends('backend.app')

@section('content')
<div class="container-fluid">
    <datatable></datatable>
</div>

@endsection

@section('js')
<script type="text/javascript">
  $('#newCustomer').on('click', function () {
     $('#newCustomerModal').modal('show');
 });
</script>
@endsection