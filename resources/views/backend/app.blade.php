<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Otopark Yönetim Paneli">
    <meta name="keywords" content="admin,dashboard">
    <meta name="author" content="GnB Tasarım">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Otopark Yönetim Paneli</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap"
    rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/backend/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/backend/assets/plugins/font-awesome/css/all.min.css" rel="stylesheet">
    <link href="/backend/assets/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="/backend/assets/plugins/select2/css/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/backend/assets/css/sweetalert2.min.css">
    <link href="/backend/assets/css/lime.css" rel="stylesheet">
    @if(\App\UserSettings::where('id', 1)->get()->first()->selected_theme == 'night'))
    <link rel="stylesheet" type="text/css" href="/backend/assets/css/admin2.css">
    @endif
    <link href="/backend/assets/css/custom.css" rel="stylesheet">
    @yield('css')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<style>
    hr {
        border: 0;
        clear: both;
        display: block;
        width: 96%;
        background-color: rgba(131, 141, 145, 1);
        height: 1px;
    }
</style>
</head>
<body>

    <div class="lime-sidebar">
        <div class="lime-sidebar-inner slimscroll">
            <ul class="accordion-menu">
                <li class="sidebar-title">
                    Otopark Kontrol
                </li>
                <li>
                    <a href="{{route('dashboard')}}" class=""><i class="material-icons">dashboard</i>Ana
                    Sayfa</a>
                </li>
                <li>
                    <a href="{{route('allTables')}}" class=""><i class="material-icons">table_chart</i>Çizelgeler</a>
                </li>
                <li>
                    <a href="{{route('tomorrow')}}" class=""><i class="material-icons">table_chart</i>Yarın Ödeme Yapacaklar</a>
                </li>
                <li>
                    <a href="{{route('smsTracking')}}" class=""><i class="material-icons">message</i>SMS Takibi</a>
                </li>
                <li>
                    <a href="{{route('siteSettings')}}" class=""><i class="material-icons">settings</i>Site Ayarları</a>
                </li>
                <li>
                    <a href="{{route('gallerySettings')}}" class=""><i class="material-icons">settings</i>Galeri Ayarları</a>
                </li>
                <li>
                    <a href="{{route('otherSettings')}}" class=""><i class="material-icons">settings</i>SMS Ayarları</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="theme-settings-sidebar">
        <div class="theme-settings-inner">
            <h3 class="theme-sidebar-title">Hızlı Menü</h3>
            <a href="/yonetim/all-tables" class="theme-item bg-success" id="new_car_entry_quick">
            <h4 class="theme-title text-white">Yenİ Araç GİRİŞ</h4>
        </a>
        <a href="#" class="theme-item bg-primary" id="dept_check_btn">
            <h4 class="theme-title text-white">Borç Sorgula</h4>
        </a>
    </div>
</div>

<div class="lime-header">
    <nav class="navbar navbar-expand-lg">
        <section class="material-design-hamburger navigation-toggle">
            <a href="javascript:void(0)" class="button-collapse material-design-hamburger__icon">
                <span class="material-design-hamburger__layer"></span>
            </a>
        </section>
        <a class="navbar-brand" href="{{route('dashboard')}}">Yönetim Paneli</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="material-icons">keyboard_arrow_down</i>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <form class="form-inline my-2 my-lg-0 search" style="float: left !important; width: 50% !important">
            <input id="new_car_entry_btn" class="form-control mr-sm-2 bg-info text-white" type="button"
            value="Müşteri Tablosu">
        </form>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle theme-settings-link" href="#">
                    <i class="material-icons">layers</i>
                </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="material-icons">more_vert</i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li><a class="dropdown-item" href="/yonetim/site-settings">Site Ayarları</a></li>
                <li><a class="dropdown-item" href="/yonetim/admin_settings">Yönetici Ayarları</a></li>
                <li class="divider"></li>
                <li><a class="dropdown-item" href="/logout">Çıkış Yap</a></li>
            </ul>
        </li>
    </ul>
</div>
</nav>
</div>

<div class="lime-container">
    <div class="lime-body" id="app">
        @yield('content')
    </div>
    <div class="lime-footer text-center" style="position: sticky;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="footer-text">{{\Carbon\Carbon::today()->year}} © GnB Tasarım Tarafından Hazırlanmıştır. Tüm Hakları Saklıdır.</span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Javascripts -->
<script src="/js/app.js"></script>
<script src="/backend/assets/plugins/bootstrap/popper.min.js"></script>
<script src="/backend/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/backend/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="/backend/assets/plugins/toastr/toastr.min.js"></script>
<script src="/backend/assets/js/lime.js"></script>
<script src="/backend/assets/plugins/select2/js/select2.full.min.js"></script>
<script src="/backend/assets/js/sweetalert2.min.js"></script>
<script src="/backend/assets/js/custom_app.js"></script>


@yield('js')
</body>
</html>
