@extends('backend.app')

@section('content')

    <div class="container">
        <form id="settings-form" action="/api/save_settings" method="POST">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Genel Ayarlar:</h4>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <label for="">Site Adı:</label>
                            <input class="form-control" value="{{\App\SiteSettings::find(1)->name}}" type="text"
                                   name="name" id="site_name">
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="">Slogan:</label>
                            <input class="form-control" value="{{\App\SiteSettings::find(1)->slogan}}" type="text"
                                   name="slogan" id="slogan">
                        </div>
                        <div class="col-md-4 form-group">
                            <label for="">Alt Bilgi Yazısı:</label>
                            <input class="form-control" type="text" value="{{\App\SiteSettings::find(1)->footer_text}}"
                                   name="footer_text" id="footer_text">
                        </div>
                        <div class="col-md-6">
                            <label for="">Site Etiketleri:</label>
                            <input class="form-control" type="text" name="keywords"
                                   value="{{\App\SiteSettings::find(1)->keywords}}" id="keywords">
                        </div>
                        <div class="col-md-6">
                            <label for="">Site Açıklaması:</label>
                            <input class="form-control" type="text" name="description"
                                   value="{{\App\SiteSettings::find(1)->description}}" id="description">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Hakkımızda Ayarları:</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="about_us_title_1">Hakkımızda Yazısı Başlığı</label>
                                <input type="text" class="form-control" name="about_us_title_1" id="about_us_title_1"
                                       value="{{\App\SiteSettings::find(1)->about_us_title_1}}">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="">Hakkımızda Yazısı:</label>
                                <textarea class="form-control" rows="18" type="text" name="about_us_1"
                                          id="about_us_1">{{\App\SiteSettings::find(1)->about_us_1}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-4 mt-4">
                            <div class="card">
                                <img src="/storage/{{\App\SiteSettings::find(1)->about_us_img_1}}" class="card-img-top"
                                     id="the_img" alt="Placeholder">
                            </div>
                            <div class="dropzone dropzone-previews" id="about_us_img"></div>
                        </div>
                        <div class="col-md-4 mt-5">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label for="">1. Neden Biz Başlığı:</label>
                                    <input class="form-control" type="text" name="whyus_1" id="whyus_1"
                                           value="{{\App\SiteSettings::find(1)->whyus_1}}">
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">1. Neden Biz Yazısı:</label>
                                        <textarea class="form-control" rows="5" type="text" name="whyus_content_1"
                                                  id="nedenbiz_icerik_1">{{\App\SiteSettings::find(1)->whyus_content_1}}</textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4 mt-5">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label for="">2. Neden Biz Başlığı:</label>
                                    <input class="form-control" type="text" name="whyus_2" id="whyus_2"
                                           value="{{\App\SiteSettings::find(1)->whyus_2}}">
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">2. Neden Biz Yazısı:</label>
                                        <textarea class="form-control" rows="5" type="text" name="whyus_content_2"
                                                  id="nedenbiz_icerik_2">{{\App\SiteSettings::find(1)->whyus_content_2}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mt-5">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label for="">3. Neden Biz Başlığı:</label>
                                    <input class="form-control" type="text" name="whyus_3" id="nedenbiz_3"
                                           value="{{\App\SiteSettings::find(1)->whyus_3}}">
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">3. Neden Biz Yazısı:</label>
                                        <textarea class="form-control" rows="5" type="text" name="whyus_content_3"
                                                  id="nedenbiz_icerik_3">{{\App\SiteSettings::find(1)->whyus_content_3}}</textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <services></services>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Sosyal Ayarlar</h4>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="">Facebook Adresi:</label>
                            <input class="form-control" type="text" name="facebook"
                                   value="{{\App\SiteSettings::find(1)->facebook}}" id="facebook">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="">İnstagram Adresi:</label>
                            <input class="form-control" type="text" value="{{\App\SiteSettings::find(1)->instagram}}"
                                   name="instagram" id="instagram">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <input id="save_all" type="submit" class="btn btn-success p-3" value="Ayarları Kaydet">
            </div>
        </form>
    </div>
@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
    <script>
        function loadCard(src) {
            document.getElementById('the_img').src = '/storage/'+src;
        }

        Dropzone.autoDiscover = false;
        $("#about_us_img").dropzone({
            url: '/api/upload-about-us',
            headers: {
                "X-CSRF-TOKEN": document.head.querySelector("[name=csrf-token]").content
            },
            paramName: 'file',
            maxFilesize: 10, // MB
            maxFiles: 1,
            dictDefaultMessage: "Yüklemek İçin Resmi Buraya Sürükleyin",
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            init: function () {
                this.on("success", function (file, response) {
                    loadCard(response.src);
                });
            }
        });
    </script>
    <script src="/backend/assets/js/jquery.form.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#settings-form').ajaxForm({
                success: function (response) {
                    swal.fire({
                        title: 'İşlem Başarılı',
                        text: 'Site Ayarları Başarıyla Kaydedildi!',
                        confirmButtonText: 'Tamam',
                        icon: 'success',
                    });
                }
            });
        });
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css">
@endsection
