@extends('backend.app')

@section('content')
    <div class="container-fluid">
        <table id="customers-table" class="mt-5 table table-striped table-bordered text-center">
            <thead class="thead-dark">
            <tr>
                <th>İşlemler</th>
                <th class="px-5">Ad Soyad</th>
                <th class="px-5">Plaka</th>
                <th>Model</th>
                <th>Giriş Tarihi</th>
                <th>Çıkış Tarihi</th>
                <th>Aylık Ücret</th>
                <th>Toplam Borç</th>
                <th>Telefon</th>
                <th>Sms Durumu</th>
            </tr>
            </thead>
            <tbody>
            @foreach($checks as $item)
                <tr>
                    <td>
                        <a href="javascript:sendSMS({{$item->customer->id}}, '{{$item->customer->name}}', '{{$item->customer->tel}}' )" class="badge badge-primary">SMS Gönder</a><br>
                        <a class="badge badge-info" href="javascript:quickSMS({{$item->customer->id}}, '{{$item->customer->name}}', {{\App\Http\Controllers\Functions::remainingDept($item->customer->id)}}, '{{$item->customer->tel}}' )">Hızlı SMS</a>
                    </td>
                    <td class="searchable">{{$item->customer->name}}</td>
                    <td class="searchable">{{$item->customer->plate}}</td>
                    <td>{{$item->customer->model}}</td>
                    <td>{{$item->check_in}}</td>
                    <td>{{$item->check_out}}</td>
                    <td><b>{{$item->customer->price}}</b></td>
                    <td><b class="text-danger">{{\App\Http\Controllers\Functions::remainingDept($item->customer->id)}}</b></td>
                    <td class="searchable">{{$item->customer->tel}}</td>
                    <td>
                        @if(\App\SmsStatus::whereDate('created_at', \Carbon\Carbon::today())->where('customer_id', $item->customer->id)->exists())
                            @php $sms = \App\SmsStatus::whereDate('created_at', \Carbon\Carbon::today())->where('customer_id', $item->customer->id)->get()->first(); @endphp
                            @if($sms->status == 'Queued')
                                <span class="btn btn-info">Gönderilmeyi Bekliyor</span>
                            @elseif($sms->status == 'Sent')
                                <span class="btn btn-success">Gönderildi</span>
                            @elseif($sms->status == 'Delivered')
                                <span class="btn btn-success">İletildi</span>
                            @elseif($sms->status == 'Undelivered')
                                <span class="btn btn-warning">İletilmesi Bekleniyor</span>
                            @elseif($sms->status == 'Failed')
                                <span class="btn btn-danger">Gönderilemedi</span>
                            @else
                                <span class="btn btn-secondary">Hazırlanıyor</span>
                            @endif
                        @else
                            <span class="btn btn-primary ">Bugün Mesaj Gönderilmedi</span>
                        @endif

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

@section('js')
    <script>
        function quickSMS(id, name, dept, tel) {
            swal.fire({
                title: 'Gönderilecek Mesaj',
                input: 'textarea',
                inputValue: "Sayın " + name + ", yarın ödenmesi gereken toplam borcunuz " + dept + "₺'dir. Bilgilerinize sunar iyi günler dileriz. {{\App\SiteSettings::first()->name}}.",
                confirmButtonText: 'Gönder',
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'İptal',
            }).then(function (response) {
                if(response.value){
                    axios.post('/api/send_sms', {
                        tel: tel,
                        id: id,
                        text: response.value,
                    }).then((response) => {
                        swal.fire({
                            title: 'İşlem Başarılı',
                            text: name + ' adlı müşteriye mesaj gönderildi!',
                            confirmButtonText: 'Tamam',
                            icon: 'success',
                        });
                    });
                }
            })
        }

        function sendSMS(id, name, tel) {
            swal.fire({
                title: 'Gönderilecek Mesaj',
                input: 'textarea',
                inputValue: 'Sayın ' + name + ', ',
                confirmButtonText: 'Gönder',
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText: 'İptal',
            }).then(function (response) {
                if(response.value){
                    axios.post('/api/send_sms', {
                        tel: tel,
                        id: id,
                        text: response.value,
                    }).then((response) => {
                        swal.fire({
                            title: 'İşlem Başarılı',
                            text: name + ' adlı müşteriye mesaj gönderildi!',
                            confirmButtonText: 'Tamam',
                            icon: 'success',
                        });
                    });
                }
            })
        }
    </script>
@endsection

