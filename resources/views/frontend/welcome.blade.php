@extends('frontend.app')

@section('content')
    <section id="intro" class="clearfix">
        <div class="container">
            <div class="intro-info">
                <h2>{{$settings->slogan}}</h2>
            </div>
        </div>
    </section>

    <main id="main">
        <section id="about">
            <div class="container">

                <header class="section-header">
                    <h3>Hakkımızda</h3>

                </header>

                <div class="row about-extra">
                    <div class="col-lg-6 wow fadeInUp">
                        <img src="/storage/{{$settings->about_us_img_1}}" class="img-fluid" alt="">
                    </div>
                    <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
                        <h4>{{$settings->about_us_title_1}}</h4>
                        <p>{{$settings->about_us_1}}.</p>
                    </div>
                </div>

            </div>
        </section>
        <section id="services" class="section-bg">
            <div class="container">

                <header class="section-header">
                    <h3>Hizmetlerimiz</h3>
                </header>

                <div class="row">
                    @php $i = 2; @endphp
                    @foreach(\App\Service::all() as $service)
                        <div class="col-md-6 col-lg-5 @if($i % 2 == 0) offset-lg-1 @endif wow bounceInUp"
                             data-wow-duration="1.4s">
                            <div class="box">
                                <div class="icon"><i class="ion-android-car" style="color: #4680ff;"></i></div>
                                <h4 class="title"><a href="">{{$service->title}}</a></h4>
                                <p class="description">{{$service->content}}</p>
                            </div>
                        </div>
                        @php $i++; @endphp
                    @endforeach

                </div>

            </div>
        </section><!-- #services -->


        <!--==========================
          Portfolio Section
        ============================-->
        <section id="portfolio" class="clearfix">
            <div class="container">

                <header class="section-header">
                    <h3 class="section-title">Galerimiz</h3>
                </header>

                <div class="row portfolio-container">

                    @foreach(\App\Gallery::orderBy('img_order')->get() as $img)
                        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                            <div class="portfolio-wrap">
                                <img src="/storage/{{$img->src}}" class="img-fluid" alt="">
                                <div class="portfolio-info">
                                    <h4><a href="#">{{$img->name}}</a></h4>
                                    <div>
                                        <a href="/storage/{{$img->src}}" data-lightbox="portfolio"
                                           data-title="{{$img->name}}" class="link-preview" title="Ön İzleme"><i
                                                    class="ion ion-eye"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </section><!-- #portfolio -->

        <!--==========================
          Why Us Section
        ============================-->
        <section id="why-us" class="wow fadeIn">
            <div class="container">
                <header class="section-header">
                    <h3>Neden {{$settings->name}}?</h3>
                </header>

                <div class="row row-eq-height justify-content-center">

                    <div class="col-lg-4 mb-4">
                        <div class="card wow bounceInUp">
                            <i class="fa fa-check"></i>
                            <div class="card-body">
                                <h5 class="card-title">{{$settings->whyus_1}}</h5>
                                <p class="card-text">{{$settings->whyus_content_1}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 mb-4">
                        <div class="card wow bounceInUp">
                            <i class="fa fa-check"></i>
                            <div class="card-body">
                                <h5 class="card-title">{{$settings->whyus_2}}</h5>
                                <p class="card-text">{{$settings->whyus_content_2}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 mb-4">
                        <div class="card wow bounceInUp">
                            <i class="fa fa-check"></i>
                            <div class="card-body">
                                <h5 class="card-title">{{$settings->whyus_3}}</h5>
                                <p class="card-text">{{$settings->whyus_content_3}}</p>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row counters">

                    <div class="col-lg-4 col-6 text-center">
                        <span data-toggle="counter-up">1374</span>
                        <p>Araca Hizmet</p>
                    </div>

                    <div class="col-lg-4 col-6 text-center">
                        <span data-toggle="counter-up">2421</span>
                        <p>Defa Araç Bakımı</p>
                    </div>

                    <div class="col-lg-4 col-6 text-center">
                        <span data-toggle="counter-up">382,981</span>
                        <p>Saat Güvenli Otopark Hizmetit</p>
                    </div>

                </div>

            </div>
        </section>


        <!--==========================
          Contact Section
        ============================-->
        <section id="contact">
            <div class="container-fluid">

                <div class="section-header">
                    <h3>Hemen İletişime Geçin</h3>
                </div>

                <div class="row wow fadeInUp">

                    <div class="col-lg-12">
                        <div class="map mb-4 mb-lg-0">
                            <div style="width: 100%">
                                <iframe
                                        width="100%"
                                        height="600"
                                        frameborder="0" style="border:0"
                                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBYV5muphg7KFW8vO65RZPO3hYBA0yqvKA
                                            &q=KARADEİZ+OTOPARK
                                            &center=40.987354,28.7791017"
                                        allowfullscreen>
                                </iframe>
                            </div>
                            <br/>
                        </div>
                    </div>

                </div>

            </div>
        </section><!-- #contact -->

    </main>
@endsection