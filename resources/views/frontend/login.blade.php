
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive Admin Dashboard Template">
    <meta name="keywords" content="admin,dashboard">
    <meta name="author" content="stacks">
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Lime - Responsive Admin Dashboard Template</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/backend/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/backend/assets/plugins/font-awesome/css/all.min.css" rel="stylesheet">

    <!-- Theme Styles -->
    <link href="/backend/assets/css/lime.css" rel="stylesheet">
    @if(\Carbon\Carbon::now()->hour > 20 OR \Carbon\Carbon::now()->hour < 8)
        <link href="/backend/assets/css/admin2.css" rel="stylesheet">
    @endif
    <link href="/backend/assets/css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page err-500">
<div class="container">
    <div class="login-container">
        <div class="row">
            <div class="col-lg-4 col-md-5 col-sm-9 lfh">
                <div class="card login-box">
                    <div class="card-body">
                        <h5 class="card-title text-center">Yönetim Paneli Giriş Sistemine Hoşgeldiniz</h5>
                        @if(isset($error))
                            <div class="alert alert-danger" role="alert">
                                {{$error}}
                            </div>
                        @endif
                        <form method="POST" action="/login">
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control" name="username" placeholder="Kullanıcı Adınızı Giriniz.">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Şifrenizi Giriniz">
                            </div>
                            <div class="custom-control custom-checkbox form-group">
                                <input type="checkbox" class="custom-control-input" id="exampleCheck1">
                                <label class="custom-control-label" for="exampleCheck1">Beni Hatırla</label>
                            </div>
                            <a href="#" class="float-left forgot-link">Şifrenizi mi Unuttunuz?</a>
                            <input type="submit" class="btn btn-primary float-right m-l-xxs" value="Giriş Yap" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Javascripts -->
<script src="/backend/assets/plugins/jquery/jquery-3.1.0.min.js"></script>
<script src="/backend/assets/plugins/bootstrap/popper.min.js"></script>
<script src="/backend/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/backend/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="/backend/assets/js/lime.min.js"></script>
</body>
</html>
