<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{$settings->name}}</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="/favicon.png" rel="icon">
    <link href="/favicon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"
          rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="/frontend/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="/frontend/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/frontend/lib/animate/animate.min.css" rel="stylesheet">
    <link href="/frontend/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="/frontend/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/frontend/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="/frontend/css/style.css" rel="stylesheet">
</head>

<body>

<!--==========================
Header
============================-->
<header id="header" class="fixed-top">
    <div class="container">

        <div class="logo float-left">
            <h4 class="text-dark mt-1"><a href="#intro" class="scrollto">{{$settings->name}}</a></h4>
        </div>

        <nav class="main-nav float-right d-none d-lg-block">
            <ul>
                <li class="active"><a href="#intro">Ana Sayfa</a></li>
                <li><a href="#about">Hakkımızda</a></li>
                <li><a href="#services">Hizmetlerimiz</a></li>
                <li><a href="#portfolio">Galerimiz</a></li>
                <li><a href="#why-us">Neden Biz</a></li>
                <li><a href="#contact">İletişim</a></li>
            </ul>
        </nav><!-- .main-nav -->

    </div>
</header><!-- #header -->

<!--==========================
  Intro Section
============================-->
@yield('content')

<!--==========================
  Footer
============================-->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 col-md-6 footer-info">
                    <h3>{{$settings->name}}</h3>
                    <p>{{$settings->footer_text}}</p>
                </div>

                <div class="offset-2 col-lg-3 col-md-6 footer-contact">
                    <h4>Sosyal Ağlarda Bizi Bulun</h4>
                    <div class="social-links">
                        <a href="{{$settings->facebook}}" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="{{$settings->instagram}}" class="instagram"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy; <strong>{{$settings->name}}</strong>. Tüm Hakları Saklıdır.
        </div>
        <div class="credits">
            GnB Tasarım Tarafından Hazırlanmıştır.
        </div>
    </div>
</footer><!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
<!-- Uncomment below i you want to use a preloader -->
<!-- <div id="preloader"></div> -->

<!-- JavaScript Libraries -->
<script src="/frontend/lib/jquery/jquery.min.js"></script>
<script src="/frontend/lib/jquery/jquery-migrate.min.js"></script>
<script src="/frontend/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/frontend/lib/easing/easing.min.js"></script>
<script src="/frontend/lib/mobile-nav/mobile-nav.js"></script>
<script src="/frontend/lib/wow/wow.min.js"></script>
<script src="/frontend/lib/waypoints/waypoints.min.js"></script>
<script src="/frontend/lib/counterup/counterup.min.js"></script>
<script src="/frontend/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="/frontend/lib/isotope/isotope.pkgd.min.js"></script>
<script src="/frontend/lib/lightbox/js/lightbox.min.js"></script>
<!-- Contact Form JavaScript File -->

<!-- Template Main Javascript File -->
<script src="/frontend/js/main.js"></script>

</body>
</html>