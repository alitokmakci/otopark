/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import datatable from "./components/datatable";
import services from "./components/services";
import gallery from "./components/gallery";
window.Vue = require('vue');

const app = new Vue({
    el: '#app',
    components: {
        datatable,
        services,
        gallery
    }
});
