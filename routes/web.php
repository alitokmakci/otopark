<?php

use Illuminate\Http\Request;

Route::get('/', function () {

	return view('frontend.welcome')->with('settings', \App\SiteSettings::first());
});

Route::get('/api/sms_status', 'ApiController@smsStatus');

Route::get('/login', 'AdminController@login')->name('login');
Route::post('/login', 'AdminController@tryLogin');
Route::group(['prefix' => '/yonetim', 'middleware' => 'admin'], function(){
	Route::get('/', 'AdminController@welcome')->name('dashboard');
	Route::get('/all-tables', 'AdminController@allTables')->name('allTables');
	Route::get('/site-settings', 'AdminController@siteSettings')->name('siteSettings');
	Route::get('/gallery-settings', 'AdminController@gallerySettings')->name('gallerySettings');
	Route::get('/tomorrow', 'AdminController@tomorrow')->name('tomorrow');
	Route::get('/customer/{id}', 'AdminController@customer');
	Route::get('/other-settings', 'AdminController@otherSettings')->name('otherSettings');
	Route::get('/sms-tracking', 'AdminController@smsTracking')->name('smsTracking');
});

Route::group(['prefix' => '/api'], function(){
	Route::post('/get_years', 'ApiController@getYears');
	Route::post('/get_data', 'ApiController@getData');
	Route::post('/add_payment', 'ApiController@addPayment');
	Route::post('/delete_customer', 'ApiController@deleteCustomer');
	Route::post('/check_out', 'ApiController@checkOut');
	Route::post('/make_date', 'ApiController@makeDate');
	Route::post('/add_new_customer', 'ApiController@addNewCustomer');
	Route::post('/send_sms', 'ApiController@sendSMS');
	Route::post('/sms_status', 'ApiController@smsStatus');
	Route::post('/daily_car_rate', 'ApiController@daily_car_rate');
	Route::post('/welcome_chart', 'ApiController@welcome_chart');
	Route::post('/update_tel', 'ApiController@update_tel');
	Route::post('/upload', 'ApiController@upload');
	Route::post('/upload-about-us', 'ApiController@uploadAbout');
	Route::post('/load_cards', 'ApiController@load_cards');
	Route::post('/delete_img', ['uses' => 'ApiController@delete_img']);
	Route::post('/img_order_save', ['uses' => 'ApiController@img_order_save']);
	Route::post('/change_img_name', ['uses' => 'ApiController@change_img_name']);
	Route::post('/save_settings', 'ApiController@saveSettings');
	Route::post('/get-gallery', 'ApiController@getGallery');
	Route::post('/get-services', 'ApiController@getServices');
	Route::post('/add-service', 'ApiController@addService');
	Route::post('/edit-service', 'ApiController@editService');
	Route::post('/delete-service', 'ApiController@deleteService');
	Route::post('/save-other-settings', 'ApiController@saveOtherSettings');
});