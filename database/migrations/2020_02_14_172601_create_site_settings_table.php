<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Schema;

class CreateSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('site_settings', function (Blueprint $table) {
		    $table->bigIncrements('id');
		    $table->string('name');
		    $table->string('keywords')->nullable();
		    $table->string('description')->nullable();
		    $table->string('slogan')->nullable();
		    $table->string('about_us_1', 1000)->nullable();
		    $table->string('about_us_title_1', 1000)->nullable();
		    $table->string('about_us_img_1')->nullable();
		    $table->string('footer_text', 1000)->nullable();
		    $table->string('facebook')->nullable();
		    $table->string('instagram')->nullable();
		    $table->string('whyus_1', 1000)->nullable();
		    $table->string('whyus_2', 1000)->nullable();
		    $table->string('whyus_3', 1000)->nullable();
		    $table->string('whyus_content_1', 1000)->nullable();
		    $table->string('whyus_content_2', 1000)->nullable();
		    $table->string('whyus_content_3', 1000)->nullable();
		    $table->timestamps();
	    });

	    DB::table('site_settings')->insert(array(
		    'name' => 'OTOPARK'
	    ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}
