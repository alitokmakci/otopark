<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Schema;

class CreateTwiliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twilios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sid')->nullable();
            $table->string('token')->nullable();
            $table->string('number')->nullable();
            $table->timestamps();
        });

        DB::table('twilios')->insert(array(
        	'sid' => null,
	        'token' => null
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twilios');
    }
}
